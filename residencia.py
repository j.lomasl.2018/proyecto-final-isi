from flask import Flask, request, jsonify
import sqlite3
from collections import defaultdict, deque
from test import *

app = Flask(__name__)

DATABASE_NAME = "residencia.db"


# Obtener conexión a db
def get_db():
    conn = sqlite3.connect(DATABASE_NAME)
    return conn


# Función para crear la base de datos
def create_tables():
    tables = [
        """
        CREATE TABLE IF NOT EXISTS Persona (
            id_usuario INTEGER PRIMARY KEY AUTOINCREMENT,
            id_microbit INTEGER,
            nombre VARCHAR(255),
            tipo VARCHAR(15),
            habitacion INTEGER,
            FOREIGN KEY (id_microbit) REFERENCES Microbit(id_microbit),
            FOREIGN KEY (habitacion) REFERENCES Sala(id_sala),
            CHECK (
                (tipo = 'usuario' AND habitacion IS NOT NULL) OR
                (tipo != 'usuario' AND habitacion IS NULL)
            )
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Sala (
            id_sala INTEGER PRIMARY KEY AUTOINCREMENT,
            nombre VARCHAR(255)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Puerta (
            id_puerta INTEGER PRIMARY KEY AUTOINCREMENT,
            id_microbit INTEGER,
            sala1 INTEGER,
            sala2 INTEGER,
            FOREIGN KEY (id_microbit) REFERENCES Microbit(id_microbit),
            FOREIGN KEY (sala1) REFERENCES Sala(id_sala),
            FOREIGN KEY (sala2) REFERENCES Sala(id_sala)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Acceso (
            id_acceso INTEGER PRIMARY KEY AUTOINCREMENT,
            usuario INTEGER NOT NULL,
            puerta INTEGER NOT NULL,
            fecha DATETIME DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY (usuario) REFERENCES Persona(id_usuario),
            FOREIGN KEY (puerta) REFERENCES Puerta(id_puerta)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS Microbit (
            id_microbit INTEGER PRIMARY KEY AUTOINCREMENT,
            tipo VARCHAR(15),
            CHECK (
                tipo IN ('usuario', 'empleado', 'puerta', 'bs')
            )
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS PermisosPuertas (
            id_permiso INTEGER PRIMARY KEY AUTOINCREMENT,
            persona INTEGER NOT NULL,
            puerta INTEGER NOT NULL,
            FOREIGN KEY (persona) REFERENCES Persona(id_usuario)
            FOREIGN KEY (puerta) REFERENCES Puerta(id_puerta)
        )
        """,
    ]
    db = get_db()
    cursor = db.cursor()
    for table in tables:
        cursor.execute(table)
    db.commit()
    db.close()


def delete_database():
    try:
        db = sqlite3.connect("residencia.db")
        cursor = db.cursor()

        cursor.execute("DROP TABLE IF EXISTS Acceso")
        cursor.execute("DROP TABLE IF EXISTS Puerta")
        cursor.execute("DROP TABLE IF EXISTS Persona")
        cursor.execute("DROP TABLE IF EXISTS Sala")
        cursor.execute("DROP TABLE IF EXISTS Microbit")
        cursor.execute("DROP TABLE IF EXISTS PermisosPuertas")

        db.commit()
        print("Todas las tablas han sido eliminadas correctamente.")
    except sqlite3.Error as e:
        print("Error al eliminar las tablas:", e)
    finally:
        db.close()


@app.route("/store-access", methods=["POST"])
def add_access():
    """
    Registra una apertura de puerta
    """
    id_microbit_user = int(request.json["mb_user"])
    id_door = int(request.json["door"])

    if not id_microbit_user or not id_door:
        response = jsonify(
            {"status": "error", "message": "Bad Request - Missing content"}
        )
        response.status_code = 400
        return response

    db = get_db()

    try:
        cursor = db.cursor()
        cursor.execute(
            """
            INSERT INTO Acceso (usuario, puerta, fecha)
            SELECT id_usuario, ?, NOW() 
            FROM Persona 
            WHERE id_microbit = ?
            """,
            (id_door, id_microbit_user),
        )
        db.commit()

        response = jsonify({"status": "success", "message": "Acceso añadido"})
        response.status_code = 201
        return response

    except sqlite3.Error as e:
        response = jsonify(
            {"status": "error", "message": "Error en la consulta SQL: " + str(e)}
        )
        response.status_code = 500
        return response
    finally:
        db.close()


@app.route("/add_permission", methods=["POST"])
def add_permission():
    id_user = int(request.json["user"])
    id_door = int(request.json["door"])

    print(f"Añadido el permiso de puerta {id_door} a la persona {id_user}")

    if not id_user or not id_door:
        response = jsonify(
            {"status": "error", "message": "Bad Request - Missing content"}
        )
        response.status_code = 400
        return response
    
    db = get_db()

    try:
        cursor = db.cursor()
        cursor.execute(
            """
            INSERT INTO PermisosPuertas (persona, puerta)
            SELECT ?, ?
            WHERE NOT EXISTS (
                SELECT 1
                FROM PermisosPuertas
                WHERE persona = ? AND puerta = ?
                )
            """,
            (id_user, id_door, id_user, id_door),
        )
        db.commit()

        response = jsonify({"status": "success", "message": "Permiso añadido"})
        response.status_code = 201
        return response

    except sqlite3.Error as e:
        response = jsonify(
            {"status": "error", "message": "Error en la consulta SQL: " + str(e)}
        )
        response.status_code = 500
        return response
    finally:
        db.close()

@app.route("/del_permission", methods=["POST"])
def del_permission():
    id_user = int(request.json["user"])
    id_door = int(request.json["door"])

    print(f"Añadido el permiso de puerta {id_door} a la persona {id_user}")

    if not id_user or not id_door:
        response = jsonify(
            {"status": "error", "message": "Bad Request - Missing content"}
        )
        response.status_code = 400
        return response
    
    db = get_db()

    try:
        cursor = db.cursor()
        cursor.execute(
            """
            DELETE FROM PermisosPuertas
            WHERE persona = ? AND puerta = ?
            """,
            (id_user, id_door),
        )
        db.commit()

        response = jsonify({"status": "success", "message": "Permiso removido"})
        response.status_code = 204
        return response

    except sqlite3.Error as e:
        response = jsonify(
            {"status": "error", "message": "Error en la consulta SQL: " + str(e)}
        )
        response.status_code = 500
        return response
    finally:
        db.close()


@app.route("/help", methods=["GET"])
def get_help():
    print(request)
    id_user = int(request.json["mb_user"])
    print("Recibida solicitud de ayuda para el usuario con microbit (" + str(id_user) + ")")
    # Posición del usuario
    user_doors = get_last_doors(id_user)

    if not user_doors or len(user_doors) < 2:
        response = jsonify(
            {"status": "success", "message": "No se pudo encontrar al usuario"}
        )
        response.status_code = 500
        return response

    user_room, user_room_name = calculate_position_from_doors(user_doors)
    print("El usuario se encuentra en la habitación (" + str(user_room) + " - " + str(user_room_name) + ")")
    if not user_room:
        response = jsonify(
            {"status": "error", "message": "No se pudo encontrar al usuario"}
        )
        response.status_code = 500
        return response

    # Posiciones de los empleados
    empleados = get_employees_last_doors()
    
    if not empleados:
        response = jsonify(
            {"status": "error", "message": "No se han encontrado empleados"}
        )
        response.status_code = 500
        return response

    if len(empleados) == 1:

        response = jsonify(
            {"status": "success", "nurse_md_id": list(empleados.keys())[0], "room_id": user_room, "room_name": user_room_name}
        )
        response.status_code = 200
        return response

    empleados_rooms = {}

    for id_empleado, puertas in empleados.items():
        if len(puertas) == 2:
            empleados_rooms[id_empleado] = calculate_position_from_doors(puertas)[0]

    # Obtener empleado más cercano
    salas_adyacentes = get_salas_adyacentes()
    closest_nurse = 0  # Primer empleado es el más cercano
    min_distance = 999
    for id_empleado, room in empleados_rooms.items():
        distancia_a_usuario = distancia_minima(salas_adyacentes, room, user_room)
        if distancia_a_usuario < min_distance:
            min_distance = distancia_a_usuario
            closest_nurse = id_empleado

    print("El enfermero más cercano es el enfermero con id (" + str(closest_nurse) + ")")
    response = jsonify({"status": "success", "nurse": closest_nurse, "room_id": user_room, "room_name": user_room_name})
    response.status_code = 200
    return response


def get_last_doors(id_user):
    """
    Devuelve las últimas dos puertas que ha cruzado un usuario, dado su id
    """
    db = get_db()
    last_two_doors = []

    try:
        cursor = db.cursor()

        cursor.execute(
            """
            SELECT Acceso.puerta AS id_puerta, 
                Puerta.sala1 AS sala1_id, 
                Sala1.nombre AS sala1_nombre, 
                Puerta.sala2 AS sala2_id, 
                Sala2.nombre AS sala2_nombre
            FROM Acceso
            INNER JOIN Puerta ON Acceso.puerta = Puerta.id_puerta
            INNER JOIN Sala AS Sala1 ON Puerta.sala1 = Sala1.id_sala
            INNER JOIN Sala AS Sala2 ON Puerta.sala2 = Sala2.id_sala
            WHERE Acceso.usuario = ?
            ORDER BY Acceso.fecha DESC
            LIMIT 2;
            """,
            (id_user,),
        )

        access_results = cursor.fetchall()

        for row in access_results:
            door_info = {
                "id_puerta": row[0],
                "sala1_id": row[1],
                "sala1_nombre": row[2],
                "sala2_id": row[3],
                "sala2_nombre": row[4],
            }
            last_two_doors.append(door_info)

    except sqlite3.Error as e:
        print("Error en la consulta SQL: " + str(e))
        return None

    finally:
        db.close()

    return last_two_doors


def get_employees_last_doors():
    """
    Devuelve las últimas puertas que han cruzado todos los empleados, incluyendo los ID DE MICROBIT de cada empleado
    """
    db = get_db()
    try:
        cursor = db.cursor()

        cursor.execute(
            """
            WITH EmployeeAccess AS (
                SELECT
                    Persona.id_usuario AS user_id,
                    Persona.id_microbit AS microbit_id,
                    Acceso.puerta AS id_puerta,
                    Puerta.sala1 AS sala1_id,
                    Sala1.nombre AS sala1_nombre,
                    Puerta.sala2 AS sala2_id,
                    Sala2.nombre AS sala2_nombre,
                    ROW_NUMBER() OVER (
                        PARTITION BY Persona.id_usuario
                        ORDER BY Acceso.fecha DESC
                        ) AS rn
                FROM Acceso
                        INNER JOIN Puerta ON Acceso.puerta = Puerta.id_puerta
                        INNER JOIN Sala AS Sala1 ON Puerta.sala1 = Sala1.id_sala
                        INNER JOIN Sala AS Sala2 ON Puerta.sala2 = Sala2.id_sala
                        INNER JOIN Persona ON Acceso.usuario = Persona.id_usuario
                WHERE Persona.tipo = 'empleado'
            )
            SELECT
                user_id,
                microbit_id,
                id_puerta,
                sala1_id,
                sala1_nombre,
                sala2_id,
                sala2_nombre
            FROM EmployeeAccess
            WHERE rn <= 2;
            """,
        )

        results = cursor.fetchall()

        last_two_doors_by_employee = {}
        for row in results:
            employee_id = row[0]
            employee_mb_id = row[1]
            door_info = {
                "puerta": row[2],
                "sala1_id": row[3],
                "sala1_nombre": row[4],
                "sala2_id": row[5],
                "sala2_nombre": row[6],
            }
            if employee_mb_id not in last_two_doors_by_employee:
                last_two_doors_by_employee[employee_mb_id] = []
            last_two_doors_by_employee[employee_mb_id].append(door_info)

    except sqlite3.Error as e:
        print("Error en la consulta SQL: " + str(e))
        return None

    finally:
        db.close()

    return last_two_doors_by_employee


def calculate_position_from_doors(last_two_doors):
    """
    Devuelve el id de la sala en que se encuentra un usuario dadas las últimas 2 puertas que ha atravesado
    """
    if not last_two_doors or len(last_two_doors) < 2:
        print("Couldn't find user")
        return None

    if last_two_doors[1]["sala1_id"] == last_two_doors[0]["sala1_id"] or last_two_doors[1]["sala2_id"] == last_two_doors[0]["sala1_id"]:
        return last_two_doors[0]["sala2_id"], last_two_doors[0]["sala2_nombre"]
    elif (
        last_two_doors[1]["sala1_id"] == last_two_doors[0]["sala2_id"] or last_two_doors[1]["sala2_id"] == last_two_doors[0]["sala2_id"]
    ):
        return last_two_doors[0]["sala1_id"],last_two_doors[0]["sala1_nombre"]
    else:
        return None  # Error : No se puede encontrar al usuario


def get_salas_adyacentes():
    """
    Devuelve un array que contiene las parejas de salas adyacentes
    """
    db = get_db()
    try:
        cursor = db.cursor()

        cursor.execute(
            """
            SELECT P.sala1, P.sala2
            FROM Puerta P
            """,
        )

        resultados = cursor.fetchall()
        return [[sala1, sala2] for sala1, sala2 in resultados]

    except sqlite3.Error as e:
        print("Error en la consulta SQL: " + str(e))
        return None
    finally:
        db.close()


def distancia_minima(parejas, num1, num2):
    """
    Algoritmo utilizado para obtener la distancia mínima entre dos salas.
    """
    # Crear un diccionario para representar el grafo de parejas
    grafo = defaultdict(list)
    for pareja in parejas:
        grafo[pareja[0]].append(pareja[1])
        grafo[pareja[1]].append(pareja[0])

    # Inicializar una cola para la búsqueda BFS y un conjunto para rastrear los nodos visitados
    cola = deque()
    visitados = set()

    # Agregar el primer número a la cola como el nodo de inicio
    cola.append((num1, 0))

    while cola:
        nodo, distancia = cola.popleft()
        visitados.add(nodo)

        # Si encontramos el segundo número, devolvemos la distancia
        if nodo == num2:
            return distancia

        # Agregar los nodos vecinos no visitados a la cola
        for vecino in grafo[nodo]:
            if vecino not in visitados:
                cola.append((vecino, distancia + 1))

    # Si no se pudo encontrar una conexión entre los dos números, devuelve -1
    return -1


def init_test_db():
    rooms = [
        {
            "name": "H1",
        },
        {
            "name": "H2",
        },
        {
            "name": "H3",
        },
        {
            "name": "P",
        },
        {
            "name": "Principal",
        },
        {
            "name": "Hall",
        },
    ]

    doors = [
        {"room1": "H1", "room2": "P"},
        {"room1": "H2", "room2": "P"},
        {"room1": "H3", "room2": "P"},
        {"room1": "P", "room2": "Principal"},
        {"room1": "Principal", "room2": "Hall"},
        {"room1": "P", "room2": "Hall"},
    ]

    users = [
        {"name": "Emilio Torrevieja", "room": 1},
        {"name": "Clara Villalba", "room": 2},
        {"name": "Teresa Gómez", "room": 3},
    ]

    empleados = [{"name": "Lucía Madariaga"}, {"name": "Rubén García"}]

    accesses = [
        # Persona 1 -> H1 - P - Principal - Hall
        {"user": 1, "door": 1},
        {"user": 1, "door": 4},
        {"user": 1, "door": 5},
        # Persona 2 -> H2 - P - Hall - Principal - P - H2
        {"user": 2, "door": 2},
        {"user": 2, "door": 6},
        {"user": 2, "door": 5},
        {"user": 2, "door": 4},
        {"user": 2, "door": 2},
        # Persona 3 -> H3 - P - Principal - P - H3
        {"user": 3, "door": 3},
        {"user": 3, "door": 4},
        {"user": 3, "door": 4},
        {"user": 3, "door": 3},
        # Persona 4 (empleado) -> Hall - Principal - P - Hall
        {"user": 4, "door": 5},
        {"user": 4, "door": 4},
        {"user": 4, "door": 6},
        # Persona 5 (empleado) -> Hall - P - Principal - P - H2
        {"user": 5, "door": 6},
        {"user": 5, "door": 4},
        {"user": 5, "door": 4},
        {"user": 5, "door": 2},
    ]

    for r in rooms:
        add_room(r["name"])

    for d in doors:
        add_door(d["room1"], d["room2"])

    for u in users:
        add_user(u["name"], "usuario", u["room"])

    for e in empleados:
        add_user(e["name"], "empleado")

    for a in accesses:
        add_acceso(a["user"], a["door"])
        time.sleep(1)


def test_database():
    delete_database()
    create_tables()
    init_test_db()

def main():
    test_database()
    app.run(debug=False, port=5000)


if __name__ == "__main__":
    main()
