import threading
import serial
import requests
import time
import random
import serial
import serial.tools.list_ports as list_ports
from flask import Flask, request, jsonify


app = Flask(__name__)

URL_RESIDENCIA = "http://localhost:5000"
PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 0.1
SERIAL_PORT = None

RECEIVED_MESSAGES = []


def generate_msg_id():
    # Genera un número aleatorio de 6 dígitos
    msg_id = random.randint(100000, 999999)
    return msg_id


# Función para encontrar el puerto al que microbit está conectado
def find_microbit(pid, vid, baud):
    serial_port = serial.Serial(timeout=TIMEOUT)
    serial_port.baudrate = baud
    ports = list(list_ports.comports())
    print("Scanning ports...")
    for p in ports:
        try:
            if (p.pid == pid) and (p.vid == vid):
                print(f"Found device at {p.device} [pid : {p.pid} | vid : {p.vid}]")
                serial_port.port = str(p.device)
                global SERIAL_PORT
                SERIAL_PORT = serial_port
        except AttributeError:
            continue


def process_msg(msg):
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]
    if content.startswith("OPEN_SUCCESS"):
        id_microbit_user = content[13:]
        id_door = data[1]
        add_user_movement(id_microbit_user, id_door)
    elif content == "ALERT":
        nurse, room = get_closest_nurse(from_id)
        if nurse and room:
            msg = f"{generate_msg_id()} 0 {nurse} HELP_{room}"
            print(f"Enviamos mensaje: '{msg}'")
            send_to_microbit(msg)
        else:
            print("Error! Couldn't get nurse")


# Función para enviar mensaje al micro:bit
def send_to_microbit(message):
    print("SEND:" + message)
    if message:
        SERIAL_PORT.write(message.encode())


# Función para procesar mensajes de micro:bit
def read_from_microbit():
    while True:
        time.sleep(0.1)
        SERIAL_PORT.flush()
        received_message = SERIAL_PORT.readline().decode("utf-8").strip()
        if received_message:
            if not received_message in RECEIVED_MESSAGES:
                RECEIVED_MESSAGES.append(received_message)
                print(received_message)
                process_msg(str(received_message))


def add_user_movement(id_microbit_user, id_door):
    print("Añadiendo movimiento de usuario...")
    response = requests.post(
        f"{URL_RESIDENCIA}/store-access", json={"mb_user": id_microbit_user, "door": id_door}
    )
    if response.status_code == 200:
        print("Fin")
    else:
        print(response + str(response.json()["message"]))


def get_closest_nurse(id_microbit_user):
    print(f"Buscando enfermero más cercano al usuario {id_microbit_user}...")
    response = requests.get(f"{URL_RESIDENCIA}/help", json={"mb_user": id_microbit_user})
    if response.status_code == 200:
        return int(response.json()["nurse"]), str(response.json()["room_name"])
    else:
        print(response + str(response.json()["message"]))
        return None


def main():
    # Encontrar y abrir puerto al que está conectado el micro:bit
    find_microbit(PID_MICROBIT, VID_MICROBIT, 115200)
    if not SERIAL_PORT:
        print("Couldn't find microbit")
        return
    print("Opening and monitoring microbit port...")
    SERIAL_PORT.open()

    # Crear hilos para procesar mensajes del micro:bit
    microbit_thread = threading.Thread(target=read_from_microbit)
    microbit_thread.start()

    # Ejecutar la app
    app.run(debug=False, port=6000)


if __name__ == "__main__":
    main()
