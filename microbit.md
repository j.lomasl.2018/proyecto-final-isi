# Guía microbit

### Uso microbit (temporal)

Pulsa el botón A para configurar el id del microbit destino (0-12).
Pulsa el botón B para configurar el id del microbit (0-12).

### Acciones

- Abrir puerta
- Cambiar clave puerta (admin)
- Alerta por caída
- Enviar enfermero a una puerta
- Añadir permiso de puerta a usuario
- Remover permiso de puerta a usuario

### Instrucciones de uso

Todos los mensajes se envían con el formato:

```
[ID_mensaje] [ID_microbit_emisor] [ID_microbit_destino] [Contenido]
```

En la siguiente tabla se especifican las posibles acciones y el formato del contenido.

|              Acción              |         Contenido         |
| :------------------------------: | :-----------------------: |
|      Solicitud abrir puerta      |          OPENREQ          |
| Respuesta solicitud abrir puerta |          OPENRES          |
|           Abrir puerta           |       OPEN\_[Clave]       |
|           Abrir puerta           | OPEN_SUCCESS\_[ID_puerta] |
|          Señal de caída          |           ALERT           |
|         Enviar enfermero         |      HELP\_[ID_sala]      |
|          Añadir puerta           |     ADD\_[ID_puerta]      |
|          Remover puerta          |     DEL\_[ID_puerta]      |


### Proceso de apertura puerta

Este proceso consta de varios pasos.

1. El microbit del usuario envía una señal de muy corto alcance buscando puertas que abrir. Este mensaje tiene un formato: "XXXXXX [ID_usuario] 0 OPENREQ".
   
2. El microbit de la puerta recibe el mensaje por radio. Lee del mensaje el ID del microbit que desea abrir la puerta, y envía un mensaje a ese microbit para que continúe con la solicitud. Este mensaje tiene un formato: "XXXXXX [ID_puerta] [ID_usuario] OPENREQ".
   
3. El usuario recibe la solicitud OPENREQ. A partir del ID de la puerta el microbit del usuario comprueba si puede acceder abrir dicha puerta. En caso negativo emite un pequeño pitido y muestra una cruz por pantalla. En caso afirmativo se envía una solicitud OPEN a la puerta. Este mensaje tiene un formato: "XXXXXX [ID_usuario] [ID_puerta] OPEN".
   
4. La puerta recibe la solicitud OPEN. Puesto que el microbit del usuario ya ha comprobado si tiene permisos para abrir la puerta, ésta puede abrirse. Para indicar que el proceso ha tenido éxito se emite un pitido. Se entiende y asume que el usuario siempre cruza la puerta una vez la abre. No hacerlo puede suponer problemas a la hora de localizarlo. Finalmente la puerta envía un mensaje a la base station para registrar el paso del usuario por esta puerta. Este mensaje tiene un formato: "XXXXXX [ID_puerta] 0 OPEN_SUCCESS_[ID_usuario]".