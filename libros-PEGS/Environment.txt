ENVIRONMENT BOOK (E)

1. Glossary:
- Micro:bit: es un dispositivo pequeño, portátil y de bajo coste que cuenta con una pantalla, un micrófono, un altavoz y una variedad de sensores, entre otros. Esto permite a los usuarios acceder a información y contenido multimedia, enviar alertas de salud y seguridad, y realizar actividades recreativas
- API: es una pieza de código que permite a diferentes aplicaciones comunicarse entre sí y compartir información y funcionalidades. Lo usaremos para que las BS se comunican entre ellos
- Flask: es un framework minimalista escrito en Python que permite crear aplicaciones web rápidamente y con un mínimo número de líneas de código
- SQLite3: Es un sistema que permite que se generen las bases de datos
- Base Station: es una instalación fija o moderada de radio para la comunicación con los micro:bit

2. Components:
- API REST: para manejar los datos producidos por el sistema de monitorización
- Base de Datos: donde se guardan las incidencias

3. Constraints:
- El dispositivo debe cumplir con los requisitos de seguridad y privacidad establecidos por la legislación vigente
- Terminales pequeños para los pacientes y enfermeros
- Acelerómetro para detectar caídas, botones para realizar solicitudes, radio para enviar mensajes y un display para poder mostrarlos
- El sistema debe ser capaz de reconocer la ubicación de los terminales dentro del espacio de la residencia
- El sistema debe tener alcance para que los terminales puedan tener cobertura en toda la residencia
- Los terminales deben poder intercambiar mensajes y mostrarlos en los receptores
- El sistema debe poder registrar todos los mensajes enviados

4. Assumptions:
- La velocidad de trasmisión de las notificaciones de los microbit debe ser lo suficientemente rapida para atender una urgencia si es necesaria
- No es necesario distinguir ubicaciones dentro de una misma 
- Se cuenta con una instalación eléctrica e Internet en la residencia
- No se requieren QoS
- Los pacientes y enfermeros llevarán asignado un terminal permanentemente
- Siempre habrá algún enfermero en la residencia

5. Effects:
- Un administrador o un enfermero debe ser capaz de modificar algun dato del paciente
- Es necesario cargar la bateria de los micro-bit cuando esta se agote

6. Invariants:
- Cada miembro de la residencia tiene que tener un micro-bit y llevarlo siempre
- Cada integrante del personal sanitario debe tener acceso a la informacion de los microbits
