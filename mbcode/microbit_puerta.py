# MICROBIT DOOR

from microbit import *
import radio
import music
import random

radio.on()
radio.config(channel=5)  # Configura el canal de radio (el mismo en ambos micro:bit)

ID_MB = 0  # Inicia el id en 0
received_messages = []  # Lista de strings
sent_messages = []  # Listade strings


def generate_msg_id():
    # Genera un número aleatorio de 6 dígitos
    msg_id = random.randint(100000, 999999)
    return msg_id


def send_message(id_msg, from_id, to_id, content):
    message = "{} {} {} {}".format(id_msg, from_id, to_id, content)
    sent_messages.append(message)
    radio.send(message)


def process_action(msg):
    global door_key
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    if content[:7] == "OPENREQ":    # Solicitud de abrir puerta
        handle_open_request(from_id)
    elif content[:4] == "OPEN": # Enviar log de apertura de puerta a bs
        send_message(generate_msg_id(), ID_MB, 0, "OPEN_SUCCESS_" + data[1])


def communicate(msg):
    global ID_MB
    global received_messages
    global sent_messages
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]
    # Si ya hemos enviado este mensaje o ya lo hemos recibido no hacemos nada
    if from_id == ID_MB or msg in received_messages:
        return
    # Soy el destinatario (también me vale 0)
    if to_id == ID_MB or to_id == 0:  # Si el mensaje no es ACK es original => Envío ACK
        if content[0:3] is not "ACK":
            process_action(msg)
            send_message(generate_msg_id(), to_id, from_id, "ACK_" + str(id_msg))
        # Recibimos un ACK, así que borramos el mensaje de la lista de enviados
        else:
            id = content[4:]
            for m in sent_messages:
                if id == m[0:7]:
                    sent_messages.remove(m)
                    break
        return
    if content[0:3] is not "ACK":
        # Recibo un mensaje normal, pero ya he recibido su ACK
        for m in received_messages:
            if "ACK" == m[-10:-7] and id_msg == m[-6:]:
                return

    received_messages.append(msg)  # Guardar mensaje recibido
    send_message(id_msg, from_id, to_id, content)  # Propagamos


def check_input():  # Método temporal para testear
    global ID_MB
    if button_a.was_pressed():
        display.scroll(str(ID_MB), delay=50)
    if button_b.was_pressed():
        ID_MB += 1
        if ID_MB == 13:
            ID_MB = 0
        display.scroll(str(ID_MB), delay=50)


def handle_open_request(id_user):
    send_message(generate_msg_id(), ID_MB, id_user, "OPENRES")


def main():
    rssi_th = -40 # Umbral abrir puerta

    while True:
        check_input()
        received_packet = radio.receive_full()  # Espera a recibir un mensaje
        if received_packet:
            # Comprobar si es solicitud de abrir puerta
            message, rssi, timestamp = received_packet
            message = str(message[3:], 'UTF-8')
            # Si es una solicitud de apertura y
            if rssi > rssi_th and message.split(" ")[3][0:4] == "OPEN":
                communicate(message)


if __name__ == "__main__":
    main()
