# MICROBIT ENFERMERO

from microbit import *
import radio
import music
import random

radio.on()
radio.config(channel=5)  # Configura el canal de radio (el mismo en ambos micro:bit)

ID_MB = 0  # Inicia el id en 0
destino = 0
received_messages = []  # Lista de strings
sent_messages = []  # Lista de strings
DOORS = set()


def generate_msg_id():
    # Genera un número aleatorio de 6 dígitos
    msg_id = random.randint(100000, 999999)
    return msg_id


def send_message(id_msg, from_id, to_id, content):
    message = "{} {} {} {}".format(id_msg, from_id, to_id, content)
    sent_messages.append(message)
    radio.send(message)


# Aquí ya hacemos lo que queramos
def process_action(msg):
    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]
    if content[0:3] == "ADD":
        music.play(music.BA_DING, wait=False)
        DOORS.add(int(content[4:]))
    elif content[0:3] == "DEL" and int(content[4:]) in DOORS:
        music.play(music.BA_DING, wait=False)
        DOORS.remove(int(content[4:]))
    elif content[:4] == "HELP":
        # Muestra dónde ir hasta que el enfermero pulse A. Bloqueante
        display.show(content[5:])
        while True:
            alert_sound()
            if button_a.was_pressed():
                display.clear()
                break


def communicate(msg):
    global ID_MB
    global received_messages
    global sent_messages

    data = msg.split(" ")
    id_msg = int(data[0])
    from_id = int(data[1])
    to_id = int(data[2])
    content = data[3]

    # Si ya hemos enviado este mensaje o ya lo hemos recibido no hacemos nada
    if from_id == ID_MB or msg in received_messages or (content[0:4] == "OPEN" and to_id != ID_MB and content[:12] != "OPEN_SUCCESS"):
        return
    # Soy el destinatario
    if to_id == ID_MB:  # Si el mensaje no es ACK es original => Envío ACK
        if content[0:3] is not "ACK":
            process_action(msg)
            send_message(generate_msg_id(), to_id, from_id, "ACK_" + str(id_msg))
        # Recibimos un ACK, así que borramos el mensaje de la lista de enviados
        else:
            id = content[4:]
            for m in sent_messages:
                if id == m[0:6]:
                    sent_messages.remove(m)
                    break
        return
    if content[0:3] is not "ACK":
        # Recibo un mensaje normal, pero ya he recibido su ACK
        for m in received_messages:
            if "ACK" == m[-10:-7] and id_msg == m[-6:]:
                return

    received_messages.append(msg)  # Guardar mensaje recibido
    send_message(id_msg, from_id, to_id, content)  # Propagamos


def check_input():  # Método temporal para testear
    global ID_MB
    global destino
    if button_a.was_pressed():
        destino += 1
        if destino == 13:
            destino = 0
        display.scroll(str(destino), delay=50)
    if button_b.was_pressed():
        ID_MB += 1
        if ID_MB == 13:
            id_mb = 0
        display.scroll(str(ID_MB), delay=50)


def alert_sound():
    freq = 1850
    duration = 200
    for i in range(2):
        music.pitch(freq,duration,pin0, True)
        music.pitch(freq,duration,pin0, True)
        music.pitch(freq,duration,pin0, True)
        music.pitch(freq,duration,pin0, True)
        sleep(250)


def main():
    while True:
        check_input()
        message = radio.receive()  # Espera a recibir un mensaje
        if message:
            communicate(message)


if __name__ == "__main__":
    main()
