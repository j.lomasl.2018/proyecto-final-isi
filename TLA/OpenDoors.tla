------------------------------ MODULE OpenDoors ------------------------------
EXTENDS Integers, Sequences

CONSTANTS Doors, Microbits, Elderly, BaseStation

VARIABLES doorState, messages

Init ==
    /\ doorState = [doorID \in Doors |-> "closed"]
    /\ messages = << >>

SendMsg(from, to, content) ==
    /\ messages' = Append(messages, <<from, to, content>>)
    /\ UNCHANGED doorState

OpenDoor(doorID) ==
    /\ doorState[doorID] = "closed"
    /\ doorState' = [doorState EXCEPT ![doorID] = "open"]
    /\ UNCHANGED messages

ReceiveMsg ==
    IF messages /= << >> THEN
        \E msg \in DOMAIN messages :
           LET from == messages[msg][1]
               to == messages[msg][2]
               content == messages[msg][3]
           IN  IF to = BaseStation THEN
                    /\ IF content = "OPENREQ" THEN
                           \E doorID \in Doors:
                               OpenDoor(doorID)
                       ELSE
                           UNCHANGED doorState
                    /\ messages' = [m \in DOMAIN messages \ {msg} |-> messages[m]]
                ELSE
                    UNCHANGED <<doorState, messages>>
    ELSE
        UNCHANGED <<doorState, messages>>

Next ==
    \/ \E doorID \in Doors : OpenDoor(doorID)
    \/ \E from \in Microbits, to \in (Microbits \cup {BaseStation}), content \in {"OPENREQ", "SOME_OTHER_MESSAGE"} : SendMsg(from, to, content)
    \/ ReceiveMsg

Spec == Init /\ [][Next]_<<doorState, messages>>

==========
